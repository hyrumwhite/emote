function init() {
  var d = new Date();
  var n = d.getDay();
  n = n % 5;
  var body = document.getElementById('main-body');
  body.style.background = "url('img/landscape" + n + ".svg')";
  body.style.backgroundSize = 'cover';
}
document.addEventListener('DOMContentLoaded',init);
var app = angular.module('emoteApp',['ngSanitize','emCard','emSearch', 'emWrite', 'emComment','webSocket','emReact', 'emNotify']);