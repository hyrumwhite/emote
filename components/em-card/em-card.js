angular.module('emCard', [])
  .factory('PostFactory', ['WebSocket','$filter', function(WebSocket, $filter) {
    var pageIndex = 0;
    var pageIncrement = 20;
    return {
      posts: [],
      populatePosts:  function(response) {
        for(var i = 0; i < response.length; i++) {
          response[i].comments = $filter('toArray')(response[i].comments);
          this.posts.push(response[i]);
        }
      },
      getPosts: function(id) {
        var self = this;
        self.posts = [];
        var request = {endpoint: "getPost"};
        request.id = id !== undefined ? id : request.id;
        WebSocket.send(request, function(response) {
          self.populatePosts(response);
        });
      },
      createPost: function(data, callback) {
        var self = this;
        data.endpoint = "createPost";
        return WebSocket.send(data, function(response) {
          self.createdPost = true;
          callback(response);
        });
      },
      enterPool: function(request) {
        request.endpoint = 'enterPool';
        WebSocket.send(request);
      },
      leavePool: function(request) {
        request.endpoint = 'leavePool';
        WebSocket.send(request);
      },
      loadMore: function() {
        var request = {};
        pageIndex += pageIncrement;
        request.endpoint = 'getNextSet';
        request.tag = window.location.hash === "" ? undefined : window.location.hash;
        request.page = pageIndex;
        var self = this;
        WebSocket.send(request, function(response) {
          self.populatePosts(response);
        });
      },
      search: function(hashtag) {
        var request = {};
        request.tag = hashtag;
        request.limit = pageIncrement;
        request.endpoint = 'search';
        var self = this;
        WebSocket.send(request, function(response) {
          self.posts = [];
          self.populatePosts(response);
        });
      }
//      flagPost: function(data) {
//        return $http.post(url + '/flag', data);
//      }
      
    };
  }])
  .directive('emCard', ['PostFactory', '$filter', function(PostFactory, $filter){
    return {
     restrict: 'E',
     replace: true,
     templateUrl: 'components/em-card/em-card.html',
     scope:{//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
       postData: "="
     },
     link: function(scope, element, attrs) {
       scope.test1 = 'asdf';
       scope.showComments = false;
       scope.showReactions = false;
       scope.postData.body = $filter('linky')(scope.postData.body, '_blank');
       scope.postData.body = $filter('hashify')(scope.postData.body, scope.postData.hashtags);
       scope.postData.body = $filter('emojify')(scope.postData.body, scope.postData._id);
//------------------------------------------------------------------------------
        scope.toggleReactions = function() {
          scope.buttonClicked = true;
          scope.showReactions = !scope.showReactions;
          if(scope.showComments === true) {
            managePools(scope.showComments, 'comments');
            scope.showComments = false;
          }
          managePools(scope.showReactions, 'reactions');
        };
        scope.getReactionCount = function() {
          var reactionCount = 0;
          for(var key in scope.postData.reactions) {
            reactionCount += scope.postData.reactions[key];
          }
          return reactionCount;
        };
//------------------------------------------------------------------------------
        scope.toggleComments = function() {
          scope.buttonClicked = true;
          scope.showComments = !scope.showComments;
          if(scope.showReactions === true) {
            managePools(scope.showReactions, 'reactions');
            scope.showReactions = false;
          }
          managePools(scope.showComments, 'comments');
        };
//------------------------------------------------------------------------------
        function managePools(toggleValue, type) {
          if(toggleValue === true) {
            PostFactory.enterPool({postId: scope.postData._id, type:type}, function(response) {
              console.log(response);
              PostFactory.commentPoolId = response.poolId;
            });
          }
          else {
            PostFactory.leavePool({postId: scope.postData._id, type:type}, function(response) {
              console.log(response);
            });            
          }          
        }
     }
   };
  }]).filter('hashify', [function(){
    return function(input, hashtags) {
      if(hashtags.length === 0) {
        return input;
      }
      else {
        var usedTags = {};
        for(var i = 0; i <  hashtags.length; i++) {
          var tag = hashtags[i];
          if(usedTags[tag] === undefined) {
            var tagRegex = new RegExp(tag,'gi');
            input = input.replace(tagRegex, '<a href="' + tag + '">' + tag + '</a>');
            usedTags[tag] = 1;
          }
        }
        return input;
      }
  
    };
  }]).filter('emojify', [function() {
    return function(input, id) {
      var emojiMap = {
        '>:(':'angry',
        "T.T":'verysad',
        '=_=':'tired',
        "@.@":"confused",
        ":'(":'sad',
        ":)":'happy',
        ':D':"veryhappy",
        ":(":'upset',
        ':O':'scared'
      };
        var emojifiedHTML = null;
        for(var key in emojiMap) {
          var rand = Math.floor(Math.random() * (11 - 1 + 1)) + 1;
          var index = input.indexOf(key);
          while(index !== -1) {
            var leftString = input.slice(0,index);
            var rightString = input.slice(index + key.length, input.length);
            emojifiedHTML = leftString + "<a href='#" + emojiMap[key] + "'><img height='21px' width='21px' src='img/" + emojiMap[key] + ".svg' class='emoji'></a>" + rightString;
            input = emojifiedHTML;
            var index = input.indexOf(key);
          }
        }
        var endString = (emojifiedHTML !== null) ? emojifiedHTML : input;
        return '<a href="#::' + id + '"><img width="50px" height="50px" src="img/profile' + rand + '.svg" class="profile"></a>' + endString;
    };
  }]);