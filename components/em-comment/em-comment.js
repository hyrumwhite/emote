angular.module('emComment', [])
  .factory('CommentFactory', ['WebSocket', function(WebSocket) {
    var url = 'http://localhost:3000/comment';
    return {
      addComment: function(comment, callback) {
        comment.endpoint = 'addComment';
        WebSocket.send(comment, callback);
      },
      likeComment: function(postId, commentId, callback) {
        console.log("liking");
        var request = {
          endpoint:'likeComment',
          commentId: commentId,
          postId: postId
        };
        WebSocket.send(request, callback);
      }
    };
  }])
  .directive('emComment', ['CommentFactory','PostFactory','$filter',  function(CommentFactory, PostFactory, $filter){
    return {
     restrict: 'E',
     replace: true,
     templateUrl: 'components/em-comment/em-comment.html',
     scope:{//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
       postId: '=',
       comments: '='
     },
     link: function(scope, element, attrs) {
       scope.linkify = function(string) {
         return $filter('linky')(string,'_blank');
       };
       scope.likeComment = function(commentId) {
         CommentFactory.likeComment(scope.postId, commentId, function(response) {
//            scope.comments[commentId].likes += 1;
         });
       };
       scope.$on('commentLiked', function(event, data) {
        if(scope.postId === data.postId) {
          var commentId = data.commentId;
          scope.$apply(function () {
            scope.comments[commentId].likes += 1;
          });
          console.log(scope.comments[commentId].likes);
        }
       });
       scope.$on('commentAdded', function(event, data) {
        for(var i = 0; i < PostFactory.posts.length; i++) {
          if(PostFactory.posts[i]._id === data.postId) {
            scope.$apply(function() {
              PostFactory.posts[i].comments = $filter('toArray')(data.comments);
              PostFactory.posts[i].commentCount = data.commentCount;
            });
          }
        }
       });
       function isWhitespaceOrEmpty(text) {
         return !/[^\s]/.test(text);
       }
       scope.postComment = function() {
         var commentInput = document.getElementById('comment-input-' + scope.postId);
         var commentBody = commentInput.textContent;
         if(isWhitespaceOrEmpty(commentBody)) {
           return;
         }
         var commentRequest = {
           postId: scope.postId,
           body: commentBody,
           likes : 0
         };
         CommentFactory.addComment(commentRequest, function(response) {
          commentInput.textContent = '';
         });
       };
     }
   };
  }]).filter('toArray', function() {
    return function(input) {
      input = input || {};
      var array = [];
      for (var key in input) {
        input[key].key = key;
        array.push(input[key]);
      }
      return array;
    };
  });