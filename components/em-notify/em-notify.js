angular.module('emNotify', [])
  .directive('emNotify', ['PostFactory', function(PostFactory) {
    return {
     restrict: 'E',
     replace: true,
     templateUrl: 'components/em-notify/em-notify.html',
     scope: {//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
     },
     link: function(scope, element, attrs) {
       var body = document.getElementById('main-body');
       var bodyScrollHeight = body.scrollTop;
       var oldDown = false;
       var oldUp = true;
       scope.newPostCount = 0;
       body.onscroll = function(event) {
         var newScrollTop = event.srcElement.scrollTop;
         var down = newScrollTop > bodyScrollHeight;
         bodyScrollHeight = newScrollTop;
         if(down === true && oldDown === false) {
            oldDown = true;
            oldUp = false;
            scope.$apply(function() {
              scope.scrollDown = true;
              scope.defaultNotify = "Back to top";
            });
         }
         else if(oldUp === false && down === false) {
          oldUp = true;
          oldDown = false;
          scope.$apply(function() {
            scope.scrollDown = false;
          });
         }
       };
       scope.loadNewPosts = function() {
         var body = document.getElementById('main-body');
         smoothScrollHack(body, 300);
         searchOnHash();
         scope.newPostCount = 0;
       };
        function searchOnHash() {
          var searchTerm = window.location.hash.replace('##','#');
          if(searchTerm.indexOf('::') !== -1) {
            PostFactory.getPosts(searchTerm.slice(3));
          }
          else {
            PostFactory.search(window.location.hash);
          }
        }
       function smoothScrollHack(element, duration) {
         var maxSpeed = 16;
         var iterations = duration/maxSpeed;
         var moveIncrement = element.scrollTop/iterations;
         var interval = window.setInterval(function(){
           if(element.scrollTop === 0){
             clearInterval(interval);
           }
           console.log('moving!');
           element.scrollTop -= moveIncrement;
         },maxSpeed);
       }
       scope.$on('postAdded', function() {
         scope.$apply(function(){
           if(!PostFactory.createdPost){
             scope.newPostCount += 1;
           }
           else {
             PostFactory.createdPost = false;
           }
         });
       });
     }
   };
  }]);