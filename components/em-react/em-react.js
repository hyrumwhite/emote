angular.module('emReact', [])
  .factory('ReactFactory', ['WebSocket', function(WebSocket) {
    var url = 'http://localhost:3000/comment';
    return {
      addReaction: function(reaction, callback) {
        reaction.endpoint = 'addReaction';
        WebSocket.send(reaction, callback);
      }
    };
  }])
  .directive('emReact', ['ReactFactory',  function(ReactFactory){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'components/em-react/em-react.html',
      scope:{//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
        postId: '=',
        reactions: '='
      },
      link: function(scope, element, attrs) {
        scope.addReaction = function(emotion) {
          var reaction = {emotion: emotion, postId:scope.postId};
          ReactFactory.addReaction(reaction, function(response) {
            console.log(response);
          });
        };
        scope.$on('reactionAdded', function(event, data) {
          if(data.postId === scope.postId) {
            for(var key in data.emotion) {
              scope.$apply(function(){scope.reactions[key] = data.emotion[key];});
            }
          }
        });
     }
   };
  }]);