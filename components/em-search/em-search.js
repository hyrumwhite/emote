angular.module('emSearch', [])
  .directive('emSearch', [function(){
    return {
     restrict: 'E',
     replace: true,
     templateUrl: 'components/em-search/em-search.html',
     scope:{//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
       'searchTerm':'='
     },
     link: function(scope, element, attrs) {
       scope.clearSearch = function() {
         scope.searchTerm = '';
         window.location.hash = '';
       };
       scope.checkSubmit = function(event) {
         if(event.which === 13) {
           window.location.hash = scope.searchTerm !== undefined ? scope.searchTerm : '';
         }
       };
     }
   };
  }]);