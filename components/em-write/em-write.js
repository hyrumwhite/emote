angular.module('emWrite', [])
  .directive('emWrite', ['PostFactory', function(PostFactory){
    return {
     restrict: 'E',
     replace: true,
     templateUrl: 'components/em-write/em-write.html',
     scope:{//camelCase = camel-case, = two-way var binding, @ text binding, & function binding
     },
     link: function(scope, element, attrs) {
       scope.showPlaceholder = true;
       function setCaret(el, position, offset) {
        var s = window.getSelection(),
            r = document.createRange();
        el.innerHTML = '\u00a0';
        r.selectNodeContents(el);
        s.removeAllRanges();
        s.addRange(r);
        document.execCommand('delete', false, null);
      }
      scope.focusWriter = function() {
        console.log('focus');
        var writer = document.getElementById('em-post-writer');
        console.log(writer);
        setCaret(writer, 0);
      };
      function isWhitespaceOrEmpty(text) {
        return !/[^\s]/.test(text);
      }
       var prompts = ["How are you feeling?", "What's up?", "Express yourself...", "Let the world know...", "Tell everyone...", 
         "Let loose...", "YOLO...", "Post away!", "Go crazy...", "Let it gooooo...."];
       scope.postLocked = true;
       function generateGreeting() {
         var rand = Math.floor(Math.random() * ((prompts.length - 1) - 0 + 1)) + 0;
         return prompts[rand];
       }
       scope.placeholder = generateGreeting();
       scope.currentPost = {
         body: '',
         comments: {},
         commentCount: 0,
         hashTags: [],
         reactionCount: 0,
         flagged: false
       };
       scope.clearPlaceholder = function() {
         scope.showPlaceholder = false;
       };
       scope.restorePlaceholder = function() {
        var text = document.getElementById('em-post-writer').textContent;
        if(isWhitespaceOrEmpty(text)) {
          scope.showPlaceholder = true;
          scope.placeholder = generateGreeting();
        }
       };
       scope.unlockPost = function(event) {
         document.getElementById('em-post-writer').style.color = '#292f33';
         if(event.which === 13 && event.shiftKey !== true) {
           scope.createPost();
         }
       };
       scope.createPost = function() {
         scope.currentPost.body = document.getElementById('em-post-writer').textContent;
         if(isWhitespaceOrEmpty(scope.currentPost.body)) {
           return;
         }
         console.log(scope.currentPost.body);
         PostFactory.createPost(scope.currentPost, function(data) {
           console.log(data);
           window.location.hash = "::" + data._id;
            var writer = document.getElementById('em-post-writer');
            writer.innerHTML = "";
            scope.placeholder = generateGreeting();
            scope.showPlaceholder = true;
         });
       };
     }
   };
  }]);