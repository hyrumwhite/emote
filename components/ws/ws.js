angular.module('webSocket', [])
  .factory('WebSocket', ['$rootScope', function($rootScope){
    var url = "ws://localhost:3001";
    var callbacks = {};
    var callbackId = 0;
    var resendData = null;
    var ws = null;
    function init() {
      ws = new WebSocket(url, "protocolOne");
      ws.onopen = function (event) {
        $rootScope.$broadcast('websocketOpen');
        if(resendData !== null) {
          ws.send(JSON.stringify(resendData));
          resendData = null;
        }
      };
      ws.onmessage = function (event) {
        try {
          var serverObj = JSON.parse(event.data);
          processData(serverObj);
        } catch(e) {
          console.log(e, event.data);
        }
      };
    }
    init();
    function processData(serverObj) {
      if(serverObj.broadcast !== undefined) {
        console.log(serverObj.broadcast);
        $rootScope.$broadcast(serverObj.broadcast, serverObj.data);
      }
      else {
        var cbid = serverObj.cbid;
        var data = serverObj.data;
        if(cbid !== undefined) {
          callbacks[cbid] && callbacks[cbid](data);
          $rootScope.$digest();
        }
      }
    }
    function generateCallbackId() {
      if(callbackId > 100000) {
        callbackId = 0;
      }
      callbackId += 1;
      return callbackId;
    }
    return {
      send: function(data, callback) {
        var callbackId = generateCallbackId();
        callbacks[callbackId] = callback;
        data.cbid = callbackId;
        if(ws.readyState === 1) {
          ws.send(JSON.stringify(data));
        }
        else {
          resendData = data;
          init();
        }
      }
    };
  }]);