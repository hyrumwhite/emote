app.controller('emoteCtrl', ['$scope', 'PostFactory',function($scope, PostFactory) {
    $scope.search = {
      searchTerm: window.location.hash
    };
    $scope.$on('websocketOpen', function(event, data) {
      searchOnHash();
    });
    $scope.postFactory = PostFactory;
    $scope.loadMore = function() {
      PostFactory.loadMore();
    };
    $scope.resetHash = function() {
      window.location.hash = '';
    };
    $scope.hideModal = function() {
      $scope.showAboutModal = false;
    };
    function searchOnHash() {
      $scope.search.searchTerm = window.location.hash.replace('##','#');
      if($scope.search.searchTerm.indexOf('::') !== -1) {
        PostFactory.getPosts($scope.search.searchTerm.slice(3));
      }
      else {
        PostFactory.search(window.location.hash);
      }
    }
    window.onhashchange = function() {
      console.log("changed!");
      searchOnHash();
    };
    document.body.classList.remove('preload');
}]);