
var jsdom = require('jsdom');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var htmlreplace = require('gulp-html-replace');
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

var scripts = [];
var css = [];
//------------------------------------------------------------------------------
gulp.task('getScripts', function(minifyScripts) {
    jsdom.env('index.html',["http://code.jquery.com/jquery.js"], function(err, window) {
      var $ = window.$;
      $("script").each(function() {
        scripts.push($(this).attr('src'));
      });
      $("link").each(function() {
        if($(this).attr('rel') === "stylesheet") {
          css.push($(this).attr('href'));
        }
      });
      scripts.pop();
      minifyScripts(null);
      gulp.src('index.html')
        .pipe(htmlreplace({
          'js':'all.min.js',
          'css':'all.min.css'
        }))
        .pipe(gulp.dest('./'));
    });
});
//------------------------------------------------------------------------------
// Concatenate & Minify JS
gulp.task('minify',['getScripts'], function() {
  console.log('Minifying: ', scripts);
  console.log('Minifying: ', css);
  gulp.src(css)
    .pipe(concat('all.min.css'))
    .pipe(gulp.dest('./'));
  return gulp.src(scripts)
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});
//------------------------------------------------------------------------------
gulp.task('default', ['getScripts', 'minify']);


// Default Task